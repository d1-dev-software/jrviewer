unit TagRWString;

interface
uses
  SysUtils,
  Tags;

type
  TTagRWString = class(TTagRW)
  private
	  FValueRd: string;
	  FValueWr: string;
    FValueWrLast: string;
  protected
	  procedure copyLastWriteToRead; override;   // ???
	  procedure copyWriteToLastWrite; override;
	  procedure copyWriteToRead; override;
  public
    constructor Create(const name: string; flags: Integer = 0);override;

    function getType: TTagType; override;
    function equalsValue(tag: TTag): Boolean; override;

    function getBool: boolean; override;
    function getInt: integer; override;
    function getLong: int64; override;
    function getDouble: double; override;
    function getString: string; override;

    procedure setBool(const value: boolean); override;
    procedure setInt(const value: integer); override;
    procedure setLong(const value: int64); override;
    procedure setDouble(const value: double); override;
    procedure setString(const value: string); override;


    function getWriteValBool: boolean; override;
    function getWriteValInt: integer; override;
    function getWriteValLong: int64; override;
    function getWriteValDouble: double; override;
    function getWriteValString: string; override;

    procedure setReadValBool(const value: boolean); override;
    procedure setReadValInt(const value: integer); override;
    procedure setReadValLong(const value: int64); override;
    procedure setReadValDouble(const value: double); override;
    procedure setReadValString(const value: string); override;
  end;


implementation

{ TTagRWString }

constructor TTagRWString.Create(const name: string; flags: Integer);
begin
  inherited Create(name, flags);
  FValueRd := '';
  FValueWr := '';
  FValueWrLast := '';
end;


function TTagRWString.getType: TTagType;
begin
  Result := ttSTRING;
end;

function TTagRWString.equalsValue(tag: TTag): Boolean;
begin
  Result := tag.getString() = FValueRd;
end;

procedure TTagRWString.copyWriteToLastWrite;
begin
		FValueWrChanged := false;
		FValueWrLast := FValueWr;
end;

procedure TTagRWString.copyLastWriteToRead;
begin
	FValueRd := FValueWrLast;
end;

procedure TTagRWString.copyWriteToRead;
begin
	FValueRd := FValueWr;
end;


// general getters
function TTagRWString.getBool: boolean;
begin
  Result := FValueRd = 'on';
end;

function TTagRWString.getInt: integer;
begin
  Result := StrToIntDef(FValueRd, 0);
end;

function TTagRWString.getLong: int64;
begin
  Result := StrToInt64Def(FValueRd, 0);
end;

function TTagRWString.getDouble: double;
begin
  Result := StrToFloatDef(FValueRd, 0.0);
end;

function TTagRWString.getString: string;
begin
  Result := FValueRd;
end;


// general setters
procedure TTagRWString.setBool(const value: boolean);
begin
  FMonitor.Enter;
  if value then
    FValueWr := 'on'
  else
    FValueWr := 'off'; // todo: consts
  FValueWrChanged := true;
  FMonitor.Leave;
end;

procedure TTagRWString.setInt(const value: integer);
begin
  FMonitor.Enter;
  FValueWr := inttostr(value);
  FValueWrChanged := true;
  FMonitor.Leave;
end;

procedure TTagRWString.setLong(const value: int64);
begin
  FMonitor.Enter;
  FValueWr := inttostr(value);
  FValueWrChanged := true;
  FMonitor.Leave;
end;

procedure TTagRWString.setDouble(const value: double);
begin
  FMonitor.Enter;
  FValueWr := floattostr(value);
  FValueWrChanged := true;
  FMonitor.Leave;
end;

procedure TTagRWString.setString(const value: string);
begin
  FMonitor.Enter;
  FValueWr := value;
  FValueWrChanged := true;
  FMonitor.Leave;
end;




// rw getters
function TTagRWString.getWriteValBool: boolean;
begin
  Result := FValueWrLast = BOOL_VALUE_TRUE;
end;

function TTagRWString.getWriteValInt: integer;
begin
  Result := StrToIntDef(FValueWrLast, 0);
end;

function TTagRWString.getWriteValLong: int64;
begin
  Result := StrToInt64Def(FValueWrLast, 0);
end;

function TTagRWString.getWriteValDouble: double;
begin
  Result := StrToFloatDef(FValueWrLast, 0.0);
end;

function TTagRWString.getWriteValString: string;
begin
  Result := FValueWrLast;
end;


// rw getters
procedure TTagRWString.setReadValBool(const value: boolean);
begin
  if value then
    FValueRd := BOOL_VALUE_TRUE
  else
    FValueRd := BOOL_VALUE_FALSE;
end;

procedure TTagRWString.setReadValInt(const value: integer);
begin
  FValueRd := inttostr(value);
end;

procedure TTagRWString.setReadValLong(const value: int64);
begin
  FValueRd := inttostr(value);
end;

procedure TTagRWString.setReadValDouble(const value: double);
begin
  FValueRd := floattostr(value);
end;

procedure TTagRWString.setReadValString(const value: string);
begin
 	FValueRd := value;
end;

end.
